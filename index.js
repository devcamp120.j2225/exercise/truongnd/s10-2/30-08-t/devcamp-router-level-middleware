 // import  thư viện express.js vào. Dạng Import express from "express";
 const express = require('express');

 // khởi tạo app express
 const app = express();

 //khai báo sẵn 1 port trên hệ thống
 const port = 8000;

// Import router Module
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");


app.use((req,res, next) => {
    let today = new Date();

    console.log("Current: ", today);

    next();
});

app.use((req,res, next) =>{
    console.log("Method: ", req.method);

    next();
});
 // Khai báo API
app.get("/", (rep,res) => {
    let today = new Date();

    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        message : `Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1 } năm ${today.getFullYear()}`
    })
});

app.use(courseRouter);
app.use(reviewRouter);

 // Khơi tạo app
 // Callback function
 app.listen(port, () =>{
    console.log("App listening on port: ",port);
 });